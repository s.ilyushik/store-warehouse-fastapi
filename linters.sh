#!/bin/bash

black .
pylint --rcfile=pylintrc ./src/*
isort .
flake8
mypy . --show-error-codes --config-file setup.cfg
