FROM python:3.11
ENV PYTHONUNBUFFERED 1

COPY ./ /app
WORKDIR /app

RUN pip install --upgrade pip
RUN pip install pipenv
RUN pip install uvicorn
RUN pipenv install --system --dev
