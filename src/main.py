from fastapi import APIRouter, FastAPI

from src.auth.router import router as auth_router
from src.operations.router import router as operations_router
from src.reports.router import router as reports_router


tags_meta_data = [
    {"name": "Operations", "description": "Operation endpoints"},
    {"name": "Authorization", "description": "User authorization endpoints"},
    {"name": "Reports", "description": "Reports endpoints"},
]

app = FastAPI(
    title="Store Warehouse",
    description="Simple edition",
    version="1.0",
    openapi_tags=tags_meta_data,
)


router = APIRouter()
router.include_router(auth_router)
router.include_router(operations_router)
router.include_router(reports_router)
app.include_router(router)
