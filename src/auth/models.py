import sqlalchemy as sa
from sqlalchemy.orm import Mapped, mapped_column

from src.auth.schemas import User
from src.core.models import BaseModel


class User(BaseModel):  # type: ignore
    """
    User model.
    """

    __tablename__ = "users"

    username: Mapped[str] = mapped_column(sa.String(35), unique=True)
    email: Mapped[str] = mapped_column(sa.Text, unique=True)
    name: Mapped[str] = mapped_column(sa.String(50), unique=False)
    password_hash: Mapped[str] = mapped_column(sa.Text)
    is_active: Mapped[bool] = mapped_column(
        sa.Boolean, default=True, nullable=False
    )
    is_superuser: Mapped[bool] = mapped_column(
        sa.Boolean, default=False, nullable=False
    )

    def to_read_model(self) -> User:
        return User(
            id=self.id,
            username=self.username,
            email=self.email,
            name=self.name,
            password_hash=self.password_hash,
            is_active=self.is_active,
            is_superuser=self.is_superuser,
        )

    def __str__(self):
        return f"{self.__class__.__name__}(id={self.id}, username={self.username!r})"

    def __repr__(self):
        return str(self)
