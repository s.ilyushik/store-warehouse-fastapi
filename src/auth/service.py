from datetime import datetime, timedelta

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from passlib.hash import bcrypt
from pydantic import ValidationError
from sqlalchemy.orm import Session

import src.auth.models
from src.auth.schemas import Token, User, UserCreate
from src.config import settings
from src.database import get_sync_session


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth/sign-in/")


def get_current_user(token: str = Depends(oauth2_scheme)) -> User:
    """
    Adding token from header to user service function.
    """
    return AuthService.validate_token(token)


class AuthService:
    """
    Main user authorization service class.
    """

    @classmethod
    def verify_password(
        cls, plain_password: str, hashed_password: str
    ) -> bool:  # noqa
        """
        Password validation method.
        """
        return bcrypt.verify(plain_password, hashed_password)

    @classmethod
    def hash_password(cls, password: str) -> str:
        """
        Password hashing method.
        """
        return bcrypt.hash(password)

    @classmethod
    def validate_token(cls, token: str) -> User:
        """
        Token validation method.
        """

        exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )

        try:
            payload = jwt.decode(
                token, settings.JWT_SECRET, algorithms=[settings.JWT_ALGORITHM]
            )
        except JWTError:
            raise exception from None

        user_data = payload.get("user")
        try:
            user = User.parse_obj(user_data)
        except ValidationError:
            raise exception from None
        return user

    @classmethod
    def create_token(cls, user: User) -> Token:
        """
        Create token by user method.
        """
        user_data = User.from_orm(user)
        now = datetime.utcnow()
        payload = {
            "iat": now,
            "nbf": now,
            "exp": now + timedelta(seconds=settings.JWT_EXPIRATION),
            "sub": str(user_data.id),
            "user": user_data.dict(),
        }
        token = jwt.encode(
            payload,
            settings.JWT_SECRET,
            settings.JWT_ALGORITHM,
        )
        return Token(access_token=token)

    def __init__(self, session: Session = Depends(get_sync_session)):
        self.session = session

    def register_new_user(self, user_data=UserCreate) -> Token:
        """
        User registration method.
        """

        user = src.auth.models.User(
            username=user_data.username,
            email=user_data.email,
            name=user_data.name,
            password_hash=self.hash_password(user_data.password),
            is_active=user_data.is_active,
            is_superuser=user_data.is_superuser,
        )
        self.session.add(user)
        self.session.commit()
        return self.create_token(user)

    def authenticate_user(self, username: str, password: str) -> Token:
        """
        User authentication method.
        """

        exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
        user = (
            self.session.query(src.auth.models.User)
            .filter(src.auth.models.User.username == username)
            .first()
        )
        if not user:
            raise exception
        if not self.verify_password(password, user.password_hash):
            raise exception
        return self.create_token(user)
