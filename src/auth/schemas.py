# pylint: disable=E0611
from pydantic import BaseModel, EmailStr


class BaseUser(BaseModel):
    """
    Base user model class.
    """

    username: str
    email: EmailStr
    name: str
    is_active: bool = True
    is_superuser: bool = False

    class Config:
        from_attributes = True


class UserCreate(BaseUser):
    """
    Create user model class.
    """

    password: str


class User(BaseUser):
    """
    User model class to retrieve.
    """

    id: int


class Token(BaseModel):
    """
    Token jwt model class.
    """

    access_token: str
    token_type: str = "bearer"
