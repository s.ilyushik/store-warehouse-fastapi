from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm

from src.auth.schemas import Token, User, UserCreate
from src.auth.service import AuthService, get_current_user


router = APIRouter(prefix="/auth", tags=["Authorization"])


@router.post("/sign-up/", response_model=Token)
def sign_up(user_data: UserCreate, service: AuthService = Depends()):
    """
    User sign up handler method.
    """
    return service.register_new_user(user_data)


@router.post("/sign-in/", response_model=Token)
def sign_in(
    form_data: OAuth2PasswordRequestForm = Depends(),
    service: AuthService = Depends(),  # noqa
):
    """
    User sign in handler method.
    """
    return service.authenticate_user(form_data.username, form_data.password)


@router.get("/user/", response_model=User)
def get_user(user: User = Depends(get_current_user)):
    """
    Current user retrieve handler method.
    """
    return user
