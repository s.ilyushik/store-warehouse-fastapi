from src.operations.main_service import MainOperationsService
from src.operations.repository import OperationRepository


def operations_service():
    """
    Operations Service Dependencies.
    """
    return MainOperationsService(OperationRepository)
