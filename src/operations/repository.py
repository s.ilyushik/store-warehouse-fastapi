from src.core.repositories.repository import SQLalchemyRepository
from src.operations.models import Operation


class OperationRepository(SQLalchemyRepository):
    """
    Operation repository.
    """

    model = Operation
