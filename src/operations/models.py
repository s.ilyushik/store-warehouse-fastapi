import datetime
from enum import Enum

import sqlalchemy as sa
from sqlalchemy.orm import Mapped, mapped_column

from src.core.models import BaseModel
from src.operations.schemas import Operation


class OperationKind(str, Enum):
    """
    Operation kind enum class.
    """

    INCOME = "income"
    OUTCOME = "outcome"


class Operation(BaseModel):  # type: ignore
    """
    Operation of income and expenses model.
    """

    __tablename__ = "operations"

    user_id: Mapped[int] = mapped_column(sa.Integer, sa.ForeignKey("users.id"))
    date: Mapped[datetime] = mapped_column(sa.DateTime)
    kind: Mapped[str] = mapped_column(sa.String(50))
    amount: Mapped[int] = mapped_column(sa.Numeric(10, 2))
    description: Mapped[str | None] = mapped_column(
        sa.String, nullable=False, default="", server_default=""
    )

    def to_read_model(self) -> Operation:
        return Operation(
            id=self.id,
            user_id=self.user_id,
            date=self.date,
            kind=self.kind,
            amount=self.amount,
            description=self.description,
            created=self.created,
            updated=self.updated,
        )
