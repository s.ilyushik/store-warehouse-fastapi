from typing import TypeVar

from src.auth.schemas import User
from src.core.logger import file_and_console_logger
from src.core.repositories.repository import AbstractRepository
from src.operations.repository import OperationRepository
from src.operations.schemas import Operation, OperationCreate


ModelType = TypeVar("ModelType")


class MainOperationsService:
    """
    Main operations service.
    """

    def __init__(self, operation_repository: type[AbstractRepository]):
        self.operation_repository: AbstractRepository = operation_repository()

    async def get_operations_list(
        self, user_id: int, filter_by: dict | None
    ) -> list[ModelType | None]:
        """
        Retrieve all users operations method.
        """
        operations = await OperationRepository().get_list(
            user_id,
            filter_by,
        )
        return operations

    async def add_operation(
        self, operation_data: OperationCreate, current_user: User
    ) -> Operation | None:
        """
        Method for adding user operation.
        """
        operation_data_dict = operation_data.model_dump()
        operation_data_dict.update(user_id=current_user.id)
        operation = await OperationRepository().create(
            operation_data_dict,
        )
        file_and_console_logger.info(
            f"Logging successful creation of an operation from "
            f"user: {current_user.name} - id: {current_user.id}."
        )

        return operation

    async def get_operation(self, filter_by: dict) -> Operation | None:
        """
        Method for getting user operation.
        """

        operation = await OperationRepository().get_by_id(filter_by)
        return operation

    async def update_operation(
        self,
        data: dict,
        filter_by: dict,
    ) -> Operation | None:
        """
        Method for updating user operation.
        """

        operation = await OperationRepository().update(
            data,
            filter_by,
        )
        return operation

    async def delete(self, current_user_pk: int, instance_id: int) -> None:
        """
        Method for deleting user operation.
        """

        operation = await OperationRepository().delete(
            current_user_pk,
            instance_id,
        )
        return operation
