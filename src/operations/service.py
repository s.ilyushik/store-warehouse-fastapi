# from typing import List
#
# from fastapi import Depends, HTTPException, status
# from sqlalchemy import select
# from sqlalchemy.orm import Session
#
# from src.database import get_async_session
# from src.operations.models import Operation
# from src.operations.schemas import (
#     OperationCreate,
#     OperationKind,
# )
#
#
# class OperationService:
#     """
#     Main operation service.
#     """
#
#     def __init__(self, session: Session = Depends(get_async_session)):
#         self.session = session
#
#     async def _get(self, user_id: int, operation_id: int) -> Operation:
#         """
#         Method for get operation by id.
#         """
#
#         operation = (
#             self.session.query(Operation)
#             .filter_by(id=operation_id, user_id=user_id)
#             .first()  # noqa
#         )
#         if not operation:
#             raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
#         return operation
#
#     async def get_list(
#         self,
#         user_id: int,
#         kind: OperationKind | None = None,
#     ) -> List[Operation]:
#         """
#         Operations service list retrieve method.
#
#         Has filtering by operations kind.
#         """
#
#         query = self.session.query(Operation).filter_by(user_id=user_id)
#         if kind:
#             query = query.filter_by(kind=kind)
#         operations = query.all()
#         return operations
#
#     async def get(self, user_id: int, operation_id: int) -> Operation:
#         """
#         Operations service retrieve by id method.
#         """
#
#         return self._get(user_id, operation_id)
#
#     # async def create(
#     #     self, user_id: int, operation_data: OperationCreate
#     # ) -> Operation:
#     #     """
#     #     Operations service create method.
#     #     """
#     #
#     #     operation = Operation(
#     #         **operation_data.dict(),
#     #         user_id=user_id,
#     #     )
#     #     self.session.add(operation)
#     #     self.session.commit()
#     #     return operation
#
#     async def create_many(
#         self, user_id: int, operations_data: List[OperationCreate]
#     ) -> List[Operation]:
#         """
#         Operations service many items create method.
#         """
#
#         operations = [
#             Operation(
#                 **operation_data.dict(),
#                 user_id=user_id,
#             )
#             for operation_data in operations_data
#         ]
#         self.session.add_all(operations)
#         self.session.commit()
#         return operations
#
#     # async def update(
#     #     self,
#     #     user_id: int,
#     #     operation_id: int,
#     #     operation_data: OperationUpdate,
#     # ) -> Operation:
#     #     """
#     #     Operations service update method.
#     #     """
#     #
#     #     operation = self._get(user_id, operation_id)
#     #     for field, value in operation_data:
#     #         setattr(operation, field, value)
#     #     self.session.commit()
#     #     return operation
#
#     async def delete(self, user_id: int, operation_id: int):
#         """
#         Operations service delete method.
#         """
#
#         operation = self._get(user_id, operation_id)
#         self.session.delete(operation)
#         self.session.commit()
