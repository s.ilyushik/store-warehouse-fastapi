from typing import Annotated, List

from fastapi import APIRouter, Depends, HTTPException, status

from src.auth.schemas import User
from src.auth.service import get_current_user
from src.operations.dependencies import operations_service
from src.operations.main_service import MainOperationsService
from src.operations.schemas import (
    Operation,
    OperationCreate,
    OperationKind,
    OperationUpdate,
)


router = APIRouter(prefix="/operations", tags=["Operations"])


@router.get("/list/", response_model=List[Operation | None])
async def get_operations_list(
    operation_service: Annotated[
        MainOperationsService, Depends(operations_service)
    ],
    kind: OperationKind | None = None,
    current_user: User = Depends(get_current_user),
):
    """
    Users operations list handler method.

    - **kind**: qwery param for filtering by enums values.
    """

    filter_by = {"kind": kind}
    operations = await operation_service.get_operations_list(
        user_id=current_user.id,
        filter_by=filter_by,
    )
    return operations


@router.post("/create/", response_model=Operation)
async def create_operation(
    operation_service: Annotated[
        MainOperationsService, Depends(operations_service)
    ],
    operation_data: OperationCreate,
    current_user: User = Depends(get_current_user),
):
    """
    Operations create handler method.
    """

    operation = await operation_service.add_operation(
        operation_data, current_user
    )
    return operation


@router.get("/{operation_id}/", response_model=Operation)
async def get_operation_by_id(
    operation_service: Annotated[
        MainOperationsService, Depends(operations_service)
    ],
    operation_id: int,
    current_user: User = Depends(get_current_user),
):
    """
    Users operations retrieve by id handler method.
    """

    filter_by = {"user_id": current_user.id, "id": operation_id}
    operation = await operation_service.get_operation(filter_by)

    if not operation:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Operation not found"
        )
    return operation


@router.put("/{operation_id}/update/", response_model=Operation)
async def update_operation(
    operation_service: Annotated[
        MainOperationsService, Depends(operations_service)
    ],
    operation_id: int,
    operation_data: OperationUpdate,
    current_user: User = Depends(get_current_user),
):
    """
    Users operations update handler method.
    """

    operation_data_dict = operation_data.model_dump()
    filter_by = {"user_id": current_user.id, "id": operation_id}
    operation = await operation_service.update_operation(
        operation_data_dict, filter_by
    )

    if not operation:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Operation not found"
        )
    return operation


@router.delete(
    "/{operation_id}/delete/", status_code=status.HTTP_204_NO_CONTENT
)
async def delete_operation(
    operation_service: Annotated[
        MainOperationsService, Depends(operations_service)
    ],
    operation_id: int,
    current_user: User = Depends(get_current_user),
) -> None:
    """
    Users operations delete handler method.
    """

    await operation_service.delete(current_user.id, operation_id)
