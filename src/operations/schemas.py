# pylint: disable=W0107, E0611
from datetime import date, datetime
from decimal import Decimal
from enum import Enum

from pydantic import BaseModel


# TODO Add schemas prefix
class OperationKind(str, Enum):
    """
    Operation kind enum class.
    """

    INCOME = "income"
    OUTCOME = "outcome"


class OperationBase(BaseModel):
    """
    Base operation model class.
    """

    date: date
    kind: OperationKind
    amount: Decimal
    description: str | None

    class Config:
        from_attributes = True


class Operation(OperationBase):
    """
    Operation model class to retrieve.
    """

    id: int
    created: datetime
    updated: datetime | None
    user_id: int


class OperationCreate(OperationBase):
    """
    Operation create model class.
    """

    pass


class OperationUpdate(OperationBase):
    """
    Operation update model class.
    """

    pass
