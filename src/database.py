from sqlalchemy import create_engine, text
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine
from sqlalchemy.orm import sessionmaker

from src.config import settings


sync_engine = create_engine(
    url=settings.database_url_sync,
    echo=False,
    echo_pool=False,
    pool_size=5,
    max_overflow=10,
)

sync_session_maker = sessionmaker(
    autocommit=False, autoflush=False, expire_on_commit=False, bind=sync_engine
)


def get_sync_session() -> sync_session_maker:
    """
    Sync session helper.
    """
    with sync_session_maker() as session:
        yield session


async_engine = create_async_engine(
    url=settings.database_url_async,
    echo=False,
    echo_pool=False,
    pool_size=5,
    max_overflow=10,
)

async_session_maker = async_sessionmaker(
    autocommit=False,
    autoflush=False,
    expire_on_commit=False,
    bind=async_engine,
)


async def get_async_session() -> async_session_maker:
    """
    Async session helper.
    """
    async with async_session_maker() as session:
        yield session
