import datetime as dt
import logging
import os

from src.config import settings


LOGGING_DIRECTORY: str = settings.LOGGING_DIRECTORY
MAX_LOG_FILE_SIZE = 1024 * 1024  # 1 MB
MAX_LOG_AGE_DAYS: int = settings.MAX_LOG_AGE_DAYS

log_folder: str = LOGGING_DIRECTORY
if not os.path.exists(log_folder):
    os.makedirs(log_folder)


def check_log_file_size(log_file_path: str) -> bool:
    """
    Function for checking whether the log file size has been exceeded.
    """
    return (
        os.path.exists(log_file_path)
        and os.path.getsize(log_file_path) < MAX_LOG_FILE_SIZE
    )


def log(message: str) -> None:
    """
    Function for log message.
    """
    log_file_path = (
        f"{log_folder}{dt.datetime.now().strftime('logs_%d.%m.%Y.txt')}"
    )
    if not check_log_file_size(log_file_path):
        log_file_path = (
            f"{log_folder}"
            f"{dt.datetime.now().strftime('logs_%d.%m.%Y_%H%M%S.txt')}"
        )

    with open(log_file_path, mode="a", encoding="utf-8") as file:
        file.write(message + "\n")


def delete_old_log_files(log_folder_path: str) -> None:
    """
    Function for deleting old log files after a certain amount of days.
    """
    now = dt.datetime.now()
    for filename in os.listdir(log_folder_path):
        filepath = os.path.join(log_folder_path, filename)

        if os.path.isfile(filepath):
            file_creation_time = dt.datetime.fromtimestamp(
                os.path.getmtime(filepath)
            )
            age = now.date() - file_creation_time.date()

            if age.days > MAX_LOG_AGE_DAYS:
                os.remove(filepath)


# Logging to file and console.
file_and_console_logger: logging.Logger = logging.getLogger(__name__)
file_and_console_logger.setLevel(logging.DEBUG)

delete_old_log_files(log_folder)


file_handler: logging.Handler = logging.FileHandler(
    f"{log_folder}{dt.datetime.now().strftime('logs_%d.%m.%Y.txt')}",
    mode="a",
    encoding="utf-8",
)
file_handler.setLevel(logging.DEBUG)

console_handler: logging.Handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)

formatter: logging.Formatter = logging.Formatter(
    "%(levelname)s: %(message)s - File name: %(filename)s - "
    "Line: %(lineno)d - Func name: %(funcName)s - Asctime: %(asctime)s",
    datefmt="%d-%m-%Y %H:%M:%S",
)
file_handler.setFormatter(formatter)
console_handler.setFormatter(formatter)

file_and_console_logger.addHandler(file_handler)
file_and_console_logger.addHandler(console_handler)


# Logging to console.
console_logger: logging.Logger = logging.getLogger("console_logger")
console_logger.setLevel(logging.DEBUG)

console_handler_only: logging.Handler = logging.StreamHandler()
console_handler_only.setLevel(logging.DEBUG)

formatter_console_only: logging.Formatter = logging.Formatter(
    "%(levelname)s: %(message)s - File name: %(filename)s - "
    "Line: %(lineno)d - Func name: %(funcName)s - Asctime: %(asctime)s",
    datefmt="%d-%m-%Y %H:%M:%S",
)

console_handler_only.setFormatter(formatter_console_only)

console_logger.addHandler(console_handler_only)
