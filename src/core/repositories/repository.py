from abc import ABC, abstractmethod
from typing import Generic, TypeVar

from sqlalchemy import delete, insert, select, update

from src.database import async_session_maker, get_async_session


ModelType = TypeVar("ModelType")


class AbstractRepository(Generic[ModelType], ABC):
    """
    Abstract class for repository.
    """

    @abstractmethod
    async def get_list(
        self,
        user_pk: int,
        filter_by: dict | None,
    ) -> list[ModelType | None]:
        """
        Abstract retrieving a list of instances.
        """
        raise NotImplementedError

    @abstractmethod
    async def create(
        self,
        data: dict,
    ) -> ModelType:
        """
        Abstract creating a new instance.
        """
        raise NotImplementedError

    @abstractmethod
    async def get_by_id(self, filter_by: dict) -> ModelType:
        """
        Abstract retrieving an instance by ID.
        """
        raise NotImplementedError

    @abstractmethod
    async def update(
        self,
        data: dict,
        filter_by: dict,
    ) -> ModelType | None:
        """
        Abstract updating an existing instance.
        """
        raise NotImplementedError

    @abstractmethod
    async def delete(self, user_pk: int, instance_id: int) -> None:
        """
        Abstract delete an instance by ID.
        """
        raise NotImplementedError


class SQLalchemyRepository(AbstractRepository[ModelType]):
    """
    SQLalchemy repository.
    """

    model = None

    async def get_list(
        self, user_id: int, filter_by: dict | None
    ) -> list[ModelType | None]:
        """
        Retrieving a list of users instances.
        """
        async with async_session_maker() as session:
            stmt = select(self.model).filter_by(user_id=user_id)
            if any(
                value is not None and value != ""
                for value in filter_by.values()
            ):
                stmt = stmt.filter_by(**filter_by)
            results = await session.execute(stmt)
            return results.scalars().all()

    async def create(
        self,
        data: dict,
    ) -> ModelType:
        """
        Creating a new instance.
        """
        async with async_session_maker() as session:
            stmt = insert(self.model).values(**data).returning(self.model)
            result = await session.execute(stmt)
            await session.commit()
            return result.scalar_one_or_none()

    async def get_by_id(
        self,
        filter_by: dict,
    ) -> ModelType | None:
        """
        Retrieving users instance by ID.
        """
        async with async_session_maker() as session:
            stmt = select(self.model).filter_by(**filter_by)
            result = await session.execute(stmt)
            return result.scalars().one_or_none()

    async def update(
        self,
        data: dict,
        filter_by: dict,
    ) -> ModelType | None:
        """
        Updating users existing instance.
        """

        async with async_session_maker() as session:
            stmt = (
                update(self.model)
                .values(**data)
                .filter_by(**filter_by)
                .returning(self.model)
            )
            result = await session.execute(stmt)
            await session.commit()
            return result.scalar_one_or_none()

    async def delete(self, current_user_pk: int, instance_id: int) -> None:
        """
        Delete users instance by ID.
        """
        async with async_session_maker() as session:
            stmt = delete(self.model).filter_by(
                user_id=current_user_pk, id=instance_id
            )
            await session.execute(stmt)
            await session.commit()
