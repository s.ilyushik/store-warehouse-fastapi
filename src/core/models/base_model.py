import datetime
from typing import Annotated

import sqlalchemy as sa
from sqlalchemy.orm import (
    DeclarativeBase,
    Mapped,
    declared_attr,
    mapped_column,
)


intpk = Annotated[int, mapped_column(primary_key=True)]

created = Annotated[
    datetime.datetime,
    mapped_column(server_default=sa.text("TIMEZONE('utc', now())")),
]
updated = Annotated[
    datetime.datetime,
    mapped_column(
        server_default=sa.text("TIMEZONE('utc', now())"),
        onupdate=datetime.datetime.utcnow,
    ),
]


class BaseModel(DeclarativeBase):
    """
    Base model class.
    """

    __abstract__ = True

    @declared_attr.directive
    def __tablename__(cls) -> str:
        return f"{cls.__name__.lower()}s"

    id: Mapped[intpk]
    created: Mapped[created]
    updated: Mapped[updated]
