import csv
from io import StringIO
from typing import Any

from fastapi import Depends

from src.operations.main_service import MainOperationsService
from src.operations.schemas import OperationCreate


# FIXME Redesign the service
class ReportsService:
    """
    Main reports service.
    """

    def __init__(self, operation_service: MainOperationsService = Depends()):
        self.operation_service = operation_service

    async def csv_import(self, user_id: int, file: Any):
        """
        СSV import method.
        """

        reader = csv.DictReader(
            (line.decode() for line in file),
            fieldnames=["date", "kind", "amount", "description"],
        )
        operations = []
        next(reader)
        for row in reader:
            operation_data = OperationCreate.parse_obj(row)
            if operation_data.description == "":
                operation_data.description = None
            operations.append(operation_data)
        await self.operation_service.create_many(user_id, operations)

    async def export_csv(self, user_id: int) -> Any:
        """
        СSV export method.
        """

        output = StringIO()
        writer = csv.DictWriter(
            output,
            fieldnames=["date", "kind", "amount", "description"],
            extrasaction="ignore",
        )
        operations = await self.operation_service.get_list(user_id)

        output.seek(0)
        return output
