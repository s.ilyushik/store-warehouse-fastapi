from fastapi import APIRouter, BackgroundTasks, Depends, File, UploadFile
from fastapi.responses import StreamingResponse

from src.auth.schemas import User
from src.auth.service import get_current_user
from src.reports.service import ReportsService


router = APIRouter(prefix="/reports", tags=["Reports"])


# FIXME Redesign the routers
@router.post("/import")
async def import_csv(
    background_tasks: BackgroundTasks,
    file: UploadFile = File(...),
    user: User = Depends(get_current_user),
    reports_service: ReportsService = Depends(),
):
    """
    Operations import from CSV file handler method.
    """
    background_tasks.add_task(
        reports_service.csv_import(),
        user.id,
        file.file,
    )


@router.get("/export")
async def export_csv(
    user: User = Depends(get_current_user),
    reports_service: ReportsService = Depends(),
):
    """
    Operations export from CSV file handler method.
    """
    report = await reports_service.export_csv(user.id)
    return StreamingResponse(
        report,
        media_type="text/csv",
        headers={"Content-Disposition": "attachment; filename=report.csv"},
    )
