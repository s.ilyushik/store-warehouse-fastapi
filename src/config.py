from pydantic import Field, PostgresDsn
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    """
    Base class of project settings.
    """

    model_config = SettingsConfigDict(
        env_file="./.env",
        env_file_encoding="utf-8",
    )

    DB_NAME: str = Field(default="postgres")
    DB_USER: str = Field(default="postgres")
    DB_HOST: str = Field(default="localhost")
    DB_PORT: int = Field(default=5432)
    DB_PASSWORD: str = Field(default="pass")

    LOGGING_DIRECTORY: str = Field(default="logs/")
    MAX_LOG_AGE_DAYS: int = Field(default=7)

    JWT_SECRET: str = Field(default="secret")
    JWT_ALGORITHM: str = Field(default="HS256")
    JWT_EXPIRATION: int = Field(default=3600)

    @property
    def database_url_async(self) -> PostgresDsn:
        """
        Settings method for get Postgres DSN asynchronous instance.
        """
        return str(
            PostgresDsn.build(
                scheme="postgresql+asyncpg",
                username=self.DB_USER,
                password=self.DB_PASSWORD,
                host=self.DB_HOST,
                path=self.DB_NAME,
                port=self.DB_PORT,
            )
        )

    @property
    def database_url_sync(self) -> PostgresDsn:
        """
        Settings method for get Postgres DSN synchronous instance.
        """
        return str(
            PostgresDsn.build(
                scheme="postgresql+psycopg2",
                username=self.DB_USER,
                password=self.DB_PASSWORD,
                host=self.DB_HOST,
                path=self.DB_NAME,
                port=self.DB_PORT,
            )
        )


settings = Settings()
