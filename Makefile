.PHONY: test pre_commit

up:  # Run docker-compose up
	docker compose -f docker-compose-dev.yml up

stop:  # Run docker-compose stop
	docker compose -f docker-compose-dev.yml stop

linters:  # Run all linters
	./linters.sh

test:  # Run pytest -ra
	@echo "Unit tests are not yet used in the project."

coverage:  # Run tests with coverage
	coverage erase
	coverage run --include=store_warehouse_FastAPI/* -m pytest -ra
	coverage report -m

pre_commit: linters test  # Run linters and tests before committing


